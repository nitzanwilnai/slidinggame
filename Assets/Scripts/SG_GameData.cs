﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "SlidingGame/GameData", order = 1)]
public class SG_GameData : ScriptableObject
{
    public Sprite floorSprite;
    public Color floorColor;

    public Sprite wallTopSprite;
    public Color wallTopColor;
    public Sprite wallBottomSprite;
    public Color wallBottomColor;

    public Sprite playerSprite;
    public Color[] playerColors;
}