﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotchOffset : MonoBehaviour {

	public float topOffset = 100.0f;

	// Use this for initialization
	void Start () {
//#if UNITY_IOS
		if (CommonFunctions.Instance.IsTallDevice())
        {
            Vector3 position = transform.localPosition;
			position.y -= topOffset;
			transform.localPosition = position;
        }
//#endif
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
