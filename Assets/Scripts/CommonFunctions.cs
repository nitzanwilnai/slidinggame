﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class WaitFor
{
    public static IEnumerator Frames(int frameCount)
    {
        while (frameCount > 0)
        {
            frameCount--;
            yield return null;
        }
    }
}

public class CommonFunctions : Singleton<CommonFunctions>
{
    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
    }
    
	public void TintMaterial(Material material, Color[] colors)
	{
		TintSprite(material, colors[0], colors[1], colors[2]);
	}

    public void TintSprite(Material material, Color redTint, Color greenTint, Color blueTint)
    {
        material.SetColor("_ColorRedTint", redTint);
        material.SetColor("_ColorGreenTint", greenTint);
        material.SetColor("_ColorBlueTint", blueTint);
    }

    public void TintPanel(GameObject panelObject, Color[] colors)
	{
		TintImage(panelObject.GetComponent<Image>(), colors);
		//Material material  = new Material(panelObject.GetComponent<Image>().material);
		//CommonFunctions.Instance.TintMaterial(material, colors);
        //panelObject.GetComponent<Image>().material = material;
	}

	public void TintButton(Button buttonObject, Color[] colors)
    {
		TintImage(buttonObject.GetComponent<Image>(), colors);
		//Material material = new Material(buttonObject.GetComponent<Image>().material);
		//CommonFunctions.Instance.TintMaterial(material, colors);
		//buttonObject.GetComponent<Image>().material = material;
    }
    
    public void TintImage(Image image, Color[] colors)
	{
		Material material = new Material(image.material);
        CommonFunctions.Instance.TintMaterial(material, colors);
        image.material = material;
	}

	public void ReplaceImageSprite(Image image, Sprite sprite)
	{
		image.sprite = sprite;
	}
    
	public void ReplacePanelSprite(GameObject panelObject, Sprite sprite)
    {
        ReplaceImageSprite(panelObject.GetComponent<Image>(), sprite);
    }

	public void ReplaceButtonSprite(Button buttonObject, Sprite sprite)
    {
        ReplaceImageSprite(buttonObject.GetComponent<Image>(), sprite);
    }

	// This property allows you to use ThisPlatform.IsIphoneX to determine if you should do anything special in your code when checking for iPhone X.
    public bool IsIPhoneX()
    {
#if UNITY_IOS

        // If testing without an iPhone X, add FORCE_IPHONEX to your Scripting Define Symbols.
        // Useful when using Xcode's Simulator, or on any other device that is not an iPhone X.
#if FORCE_IPHONEX
            return true;
#else

            // iOS.Device.generation doesn't reliably report iPhone X (sometimes it's "unknown"), but it's there in case it ever properly works.
            if (UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneX) {
                return true;
            }

            // As a last resort to see if the device is iPhone X, check the reported device model/identifier.
            string deviceModel = SystemInfo.deviceModel;
            if (deviceModel == IphoneX.Identifier_A || deviceModel == IphoneX.Identifier_B) {
                return true;
            }
            
            return false;
#endif
#else
        return false;
#endif
    }

	public bool IsTallDevice()
	{
		Debug.LogFormat("Screen width {0} x Screen height {1}", Screen.width, Screen.height);

		float screenRatio = (float)Screen.width / (float)Screen.height;
		Debug.LogFormat("IsTallDevice() {0}/{1} = {2}", (float)Screen.width, (float)Screen.height, ((float)Screen.width / (float)Screen.height));
		return (screenRatio < 0.56f); // 9/16 is 0.5625, iPhone 7/8 is slightly lower
	}

	public bool IsWideDevice()
	{
		float screenRatio = (float)Screen.width / (float)Screen.height;
		// 2/3 is 0.66, most devices this wide with high res are iPads or other big tablets
        // 0.74f because floating point is stupind and 3/4 is something smaller than 0.75.
		return ((screenRatio >= (0.66f) && Screen.width >= 1024) || (screenRatio >= 0.74f));
	}

	public float CalculateVectorAngle(Vector3 direction, Vector3 forward)
    {
        float angle = Mathf.Acos(Vector3.Dot(direction, forward)) * Mathf.Rad2Deg;
        if (direction.x < 0.0f)
        {
            angle = -angle;
        }

        return angle;
    }

    public Texture2D RenderToTexture(Camera renderCamera)
    {
        int textureWidth = renderCamera.targetTexture.width;
        int textureHeight = renderCamera.targetTexture.height;

        Texture2D finalTexture = new Texture2D(textureWidth, textureHeight, TextureFormat.RGBA32, false);

        renderCamera.Render();
        RenderTexture.active = renderCamera.targetTexture;
        finalTexture.ReadPixels(new Rect(0, 0, textureWidth, textureHeight), 0, 0);
        finalTexture.Apply();
        RenderTexture.active = null;

        return finalTexture;
    }
}

#if UNITY_IOS

public class IphoneX {

        // If unable to use Unity's Screen.safeArea, or if you just don't want to, the following values should help with your calculations to keep elements away from the screen edges (assuming the phone is landscape here).
        public const float SideMarginPercentage = .055f;
        public const float BottomMarginPercentage = .0827f;
        public const float HomeButtonWidthPercentage = .256f;

        // Device identifiers from https://www.theiphonewiki.com/wiki/List_of_iPhones
        public const string Identifier_A = "iPhone10,3";
        public const string Identifier_B = "iPhone10,6";

    }

#endif
