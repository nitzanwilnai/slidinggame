﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SG_GameBoard : MonoBehaviour
{
    public enum TileType {  NONE, START, FLOOR, BLOCK };
    public enum GameState { MAIN_MENU, IN_GAME_INPUT, IN_GAME_MOVE, IN_GAME_LEVEL_CHECK, IN_GAME_LEVEL_COMPLETE };
    GameState gameState = GameState.IN_GAME_INPUT;

    public int tilesWide = 8;
    public int tilesHigh = 8;

    [Header ("Fill in Inspector")]
    public SG_GameData gameData;
    public GameObject floorPrefab;
    public GameObject playerPrefab;
    public GameObject floorParent;
    public float moveScale = 0.64f;
    public Vector3 tileScale = Vector3.one;
    public GameObject levelComplete;
    public Text levelText;


    public TileType[,] tileTypes;
    public GameObject[,] boardTiles;
    public int[,] visited;
    GameObject player;
    int playerX;
    int playerY;
    int targetX;
    int targetY;
    int diffX;
    int diffY;

    float levelCompleteTime = 2.0f;
    float levelCompleteTimer = 0.0f;

    int colorIndex;

    // mouse movement
    bool mouseDown = false;
    Vector3 mouseStartPosition = Vector3.zero;
    Vector3 mousePrevPosition = Vector3.zero;

    int currentLevel = 0;

    List<int> levelSeeds = new List<int>();

    bool generateLevels = false;
    int numLevelsToGenerate = 10000;
    int minTiles = 85;
    int[] tileCounter;

    private void Awake()
    {
        levelComplete.SetActive(false);
        tileCounter = new int[tilesWide * tilesHigh];
    }

    // Start is called before the first frame update
    void Start()
    {
        tileTypes = new TileType[tilesWide, tilesHigh];
        boardTiles = new GameObject[tilesWide, tilesHigh];
        visited = new int[tilesWide, tilesHigh];

        CreateTiles();


        levelSeeds.Clear();
        if (generateLevels)
        {
            PlayerPrefs.SetInt("currentLevel", 0);

            currentLevel = 0;

            List<int> numTiles = new List<int>();
            List<string> levelStrings = new List<string>();
            int counter = 0;
            while(levelSeeds.Count < numLevelsToGenerate)
            {
                // loop breaker
                if (counter > numLevelsToGenerate * 1000)
                    break;

                NextLevel(counter);

                int totalFloor = 0;
                string levelString = "";
                for (int x = 0; x < tilesWide; x++)
                {
                    for (int y = 0; y < tilesHigh; y++)
                    {
                        levelString += ((int)tileTypes[x, y]).ToString();
                        if (tileTypes[x, y] == TileType.FLOOR)
                            totalFloor++;
                    }
                }
                tileCounter[totalFloor]++;

                if (totalFloor > minTiles && !levelStrings.Contains(levelString))
                {
                    levelStrings.Add(levelString);
                    numTiles.Add(totalFloor);
                    Debug.LogFormat("{0} - totalFloor {1}", counter, totalFloor);
                    levelSeeds.Add(counter);
                }
                counter++;
            }

            int largestCount = 0;
            for (int i = 0; i < tileCounter.Length; i++)
                if (tileCounter[i] > 0)
                    largestCount = i;
            Debug.LogFormat("total levels {0} largestCount {1} minTiles {2}", levelSeeds.Count, largestCount, minTiles);

            string seedString = "";
            for(int i = 0; i < levelSeeds.Count; i++)
                seedString += levelSeeds[i] + ",";
            Debug.Log(seedString);

            SaveLevels();
            NextLevel(levelSeeds[currentLevel]);
        }
        else
        {
            LoadLevels();
            currentLevel = PlayerPrefs.GetInt("currentLevel");
            NextLevel(levelSeeds[currentLevel]);
            UpdateLevelText();
        }
    }

    public void UpdateLevelText()
    {
        levelText.text = "Level " + currentLevel;
    }

    public void CreateTiles()
    {
        float offsetX = (tilesWide / 2f) * moveScale * tileScale.x - ((moveScale * tileScale.x) / 2f);
        float offsetY = (tilesHigh / 2f) * moveScale * tileScale.y - ((moveScale * tileScale.y) / 2f);

        for (int x = 0; x < tilesWide; x++)
        {
            for (int y = 0; y < tilesHigh; y++)
            {
                GameObject tile = Instantiate(floorPrefab);
                tile.transform.SetParent(floorParent.transform);
                tile.transform.localScale = tileScale;
                tile.transform.localPosition = new Vector3(x * moveScale * tileScale.x - offsetX, y * moveScale * tileScale.y - offsetY, 0.0f);
                tile.name = string.Format("Tile {0},{1}", x, y);

                boardTiles[x, y] = tile;
            }
        }

        player = Instantiate(playerPrefab);
        player.transform.SetParent(floorParent.transform);
        player.transform.localScale = tileScale * 0.9f;
        player.GetComponent<SpriteRenderer>().sprite = gameData.playerSprite;
        player.name = "Player";
    }

    public void CreateLevel(int levelSeed)
    {
        playerX = 1;
        playerY = 1;

        Random.InitState(levelSeed);

        GenerateBoard2(256);

        if (!generateLevels)
        {
            List<Vector2> floors = new List<Vector2>();
            for (int x = 0; x < tilesWide; x++)
            {
                for (int y = 0; y < tilesHigh; y++)
                {
                    if (IsValidMove(x, y))
                    {
                        floors.Add(new Vector2(x, y));
                        visited[x, y] = 0;
                    }
                    else
                        visited[x, y] = 99;
                }
            }
            int randomIndex = Random.Range(0, floors.Count);
            playerX = (int)floors[randomIndex].x;
            playerY = (int)floors[randomIndex].y;
            tileTypes[playerX, playerY] = TileType.START;

            AddTilesToBoard();
            SetWalls();

            colorIndex = Random.Range(0, gameData.playerColors.Length);
            Color playerColor = gameData.playerColors[colorIndex] * 0.75f;
            playerColor.a = 1f;
            player.GetComponent<SpriteRenderer>().color = playerColor;

            PlacePlayer(playerX, playerY);
        }
        else
        {

        }
    }

    void SaveLevels()
    {

        using (FileStream fileStream = new FileStream(Application.dataPath + "/Resources/levels.bytes", FileMode.Create, FileAccess.Write))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStream, levelSeeds);
            fileStream.Close();
        }
        using (FileStream fileStream = new FileStream(Application.dataPath + "/levels.bytes", FileMode.Create, FileAccess.Write))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStream, levelSeeds);
            fileStream.Close();
        }
    }

    void LoadLevels()
    {
        TextAsset asset = Resources.Load<TextAsset>("levels");

        BinaryFormatter binaryFormatter = new BinaryFormatter();
        Stream s = new MemoryStream(asset.bytes);
        levelSeeds = (List<int>)binaryFormatter.Deserialize(s);
    }

    public void NextLevel(int levelSeed)
    {
        CreateLevel(levelSeed);
        gameState = GameState.IN_GAME_INPUT;
    }

    public void PlacePlayer(int x, int y)
    {
        playerX = x;
        playerY = y;

        Vector3 position = boardTiles[x, y].transform.localPosition;
        position.z = -1f;
        player.transform.localPosition = position;

        boardTiles[x, y].GetComponent<SpriteRenderer>().color = gameData.playerColors[colorIndex];
        ParticleSystem ps = boardTiles[x, y].GetComponentInChildren<ParticleSystem>();
        var psMain = ps.main;
        psMain.startColor = gameData.playerColors[colorIndex];
        ps.Emit(1);
        visited[x, y] = 1;
    }

    public void GenerateBoardSetup()
    {
        for (int x = 0; x < tilesWide; x++)
        {
            for (int y = 0; y < tilesHigh; y++)
            {
                if (x == 0 || x == tilesWide - 1 || y == 0 || y == tilesHigh - 1)
                    tileTypes[x, y] = TileType.BLOCK;
                else
                    tileTypes[x, y] = TileType.NONE;

                visited[x, y] = 0;
            }
        }
    }

    public void GenerateBoard2(int numMoves)
    {
        GenerateBoardSetup();

        playerX = Random.Range(1, tilesWide - 1);
        playerY = Random.Range(1, tilesHigh - 1);

        tileTypes[playerX, playerY] = TileType.FLOOR;
        visited[playerX, playerY]++;
        int x = playerX;
        int y = playerY;

        int dx = 0;
        int dy = 0;
        // pick first direction
        int previousDirection = Random.Range(0, 4);
        switch(previousDirection)
        {
            case 0:
                dx = 1;
                break;
            case 1:
                dx = -1;
                break;
            case 2:
                dy = 1;
                break;
            case 3:
                dy = -1;
                break;

        }
        if (IsTileType(x - dx, y - dy, TileType.NONE))
            tileTypes[x - dx, y - dy] = TileType.BLOCK;

        int currentMove = 0;
        int numSteps = 0;
        while (currentMove < numMoves)
        {
            // did we run into a block? Change direction
            if (IsTileType(x + dx, y + dy, TileType.BLOCK))
            {
                numSteps = 0;
                List<Vector3> possibleDirections = new List<Vector3>();

                int[] directionCounts = new int[4];

                if(previousDirection >= 2)
                {
                    if (previousDirection != 0 && DirectionOk(x, y, 1, 0))
                    {
                        directionCounts[0] = CountDirection(x, y, 1, 0);
                        possibleDirections.Add(new Vector3(1, 0, 0));
                    }
                    if (previousDirection != 1 && DirectionOk(x, y, -1, 0))
                    {
                        directionCounts[1] = CountDirection(x, y, -1, 0);
                        possibleDirections.Add(new Vector3(-1, 0, 1));
                    }
                }
                else if(previousDirection < 2)
                {
                    if (previousDirection != 2 && DirectionOk(x, y, 0, 1))
                    {
                        directionCounts[2] = CountDirection(x, y, 0, 1);
                        possibleDirections.Add(new Vector3(0, 1, 2));
                    }
                    if (previousDirection != 3 && DirectionOk(x, y, 0, -1))
                    {
                        directionCounts[3] = CountDirection(x, y, 0, -1);
                        possibleDirections.Add(new Vector3(0, -1, 3));
                    }
                }

                if(possibleDirections.Count > 0)
                {
                    int largestCount = 0;
                    int bestDirection = 0;
                    for(int i = 0; i < directionCounts.Length; i++)
                    {
                        if (directionCounts[i] > largestCount)
                        {
                            largestCount = directionCounts[i];
                            bestDirection = i;
                        }
                    }
                    switch(bestDirection)
                    {
                        case 0:
                            dx = 1;
                            dy = 0;
                            break;
                        case 1:
                            dx = -1;
                            dy = 0;
                            break;
                        case 2:
                            dx = 0;
                            dy = 1;
                            break;
                        case 3:
                            dx = 0;
                            dy = -1;
                            break;
                    }
                    previousDirection = bestDirection;

                    //Vector3 direction = possibleDirections[Random.Range(0, possibleDirections.Count)];
                    //dx = (int)direction.x;
                    //dy = (int)direction.y;
                    //previousDirection = (int)direction.z;
                }
                else
                {
                    break;
                }

                if (IsTileType(x - dx, y - dy, TileType.NONE))
                    tileTypes[x - dx, y - dy] = TileType.BLOCK;
            }

            if(IsTileType(x + dx, y + dy, TileType.NONE) || IsTileType(x + dx, y + dy, TileType.FLOOR))
            {
                // is the next tile emppty, randomly decide to stop here
                //if (numSteps > 1 && Random.value < numSteps * 0.025f && IsTileType(x + dx, y + dy, TileType.NONE) && visited[x, y] > 0)
                //if (numSteps > 1 && Random.value < numSteps * 0.025f && IsTileType(x + dx, y + dy, TileType.NONE))
                float randomValue = Random.value;
                float magicNumber = 0.025f; // 0.025f
                if (numSteps >= 1 && randomValue < numSteps * magicNumber && IsTileType(x + dx, y + dy, TileType.NONE))
                {
                    tileTypes[x + dx, y + dy] = TileType.BLOCK;
                    numSteps = 0;
                }
                else
                {
                    numSteps++;
                    x += dx;
                    y += dy;
                    tileTypes[x, y] = TileType.FLOOR;
                    visited[x, y]++;
                }

            }

            currentMove++;
        }
    }

    public int CountDirection(int x, int y, int dx, int dy)
    {
        int counter = 0;
        int currentX = x;
        int currentY = y;
        do
        {
            currentX += dx;
            currentY += dy;
            counter++;
        }
        while ((currentX >= 0 && currentX < tilesWide) && (currentY >= 0 && currentY < tilesHigh) && tileTypes[currentX, currentY] != TileType.BLOCK);
        return counter;
    }

    public bool DirectionOk(int x, int y, int dx, int dy)
    {
        if ((IsTileType(x - dx, y - dy, TileType.BLOCK) || IsTileType(x - dx, y - dy, TileType.NONE)) && (IsTileType(x + dx, y + dy, TileType.NONE) || IsTileType(x + dx, y + dy, TileType.FLOOR)))
            return true;
        return false;
    }

    public void AddTilesToBoard()
    {
        for (int x = 0; x < tilesWide; x++)
        {
            for (int y = 0; y < tilesHigh; y++)
            {
                GameObject tile = boardTiles[x, y];
                switch (tileTypes[x, y])
                {
                    case TileType.NONE:
                        tile.GetComponent<SpriteRenderer>().color = gameData.wallTopColor;
                        break;
                    case TileType.START:
                        tile.GetComponent<SpriteRenderer>().color = gameData.floorColor;
                        break;
                    case TileType.FLOOR:
                        tile.GetComponent<SpriteRenderer>().color = gameData.floorColor;
                        break;
                    case TileType.BLOCK:
                        tile.GetComponent<SpriteRenderer>().color = gameData.wallTopColor;
                        break;
                }
            }
        }

    }

    public void SetWalls()
    {
        for (int x = 0; x < tilesWide; x++)
        {
            for (int y = 0; y < tilesHigh; y++)
            {
                if (tileTypes[x, y] == TileType.FLOOR)
                    boardTiles[x, y].GetComponent<SpriteRenderer>().sprite = gameData.floorSprite;
                if (tileTypes[x, y] == TileType.NONE || tileTypes[x, y] == TileType.BLOCK)
                {
                    bool wallBottom = false;
                    if (y == 0)
                        wallBottom = true;
                    else if (tileTypes[x, y - 1] == TileType.FLOOR)
                        wallBottom = true;

                    if(wallBottom)
                        boardTiles[x, y].GetComponent<SpriteRenderer>().sprite = gameData.wallBottomSprite;
                    else
                        boardTiles[x, y].GetComponent<SpriteRenderer>().sprite = gameData.wallTopSprite;
                }
            }
        }

    }

    bool IsTileType(int x, int y, TileType tileType)
    {
        if ((x >= 0 && x < tilesWide) && (y >= 0 && y < tilesHigh))
            if (tileTypes[x, y] == tileType)
                return true;

        return false;
    }

    bool IsValidMove(int x, int y)
    {
        if ((x >= 0 && x < tilesWide) && (y >= 0 && y < tilesHigh))
            if (tileTypes[x, y] == TileType.FLOOR || tileTypes[x, y] == TileType.START)
                return true;

        return false;
    }

    void SetGameState(GameState newGameState)
    {
        gameState = newGameState;

        if (gameState == GameState.IN_GAME_LEVEL_CHECK)
        {
            bool emptyFloorFound = false;
            for (int x = 0; x < tilesWide; x++)
            {
                for (int y = 0; y < tilesHigh; y++)
                {
                    if (visited[x, y] == 0)
                        emptyFloorFound = true;
                }
            }

            if (emptyFloorFound)
                SetGameState(GameState.IN_GAME_INPUT);
            else
                SetGameState(GameState.IN_GAME_LEVEL_COMPLETE);
        }
        else if(gameState == GameState.IN_GAME_LEVEL_COMPLETE)
        {
            currentLevel = (currentLevel + 1) % levelSeeds.Count;
            PlayerPrefs.SetInt("currentLevel", currentLevel);

            levelComplete.SetActive(true);
            levelCompleteTimer = levelCompleteTime;

            UpdateLevelText();
        }
    }

    public void SkipLevel()
    {
        currentLevel = (currentLevel + 1) % levelSeeds.Count;
        NextLevel(levelSeeds[currentLevel]);
    }

    // Update is called once per frame
    void Update()
    {
        if(gameState == GameState.IN_GAME_INPUT)
        {
#if FALSE && UNITY_IOS && !UNITY_EDITOR
            if (Input.touchCount <= 0)
            {
                return;
            }
            Touch touch = Input.GetTouch(0);

            if ((Input.GetTouch(0).phase == TouchPhase.Began))
            {
                mouseDown = true;
                mousePrevPosition = Input.GetTouch(0).position;
            }
#else
            if (Input.GetMouseButtonDown(0))
            {
                mouseDown = true;
                mousePrevPosition = Input.mousePosition;
            }
#endif

#if FALSE && UNITY_IOS && !UNITY_EDITOR
            if (mouseDown && (touch.phase == TouchPhase.Moved))
            {
                Vector3 diff = Input.GetTouch(0).position;
                diff -= mousePrevPosition;
                mousePrevPosition = Input.GetTouch(0).position;
#else
            if (mouseDown && Input.GetMouseButton(0))
            {
                Vector3 diff = Input.mousePosition;
                diff -= mousePrevPosition;
                mousePrevPosition = Input.mousePosition;
#endif
                float minMovement = 1.0f; // 10f
                diff *= 10f;
                Debug.LogFormat("diff {0} Input.minMovement {1}", diff, minMovement);
                if (Mathf.Abs(diff.x) > minMovement || Mathf.Abs(diff.y) > minMovement)
                {
#if FALSE && UNITY_IOS && !UNITY_EDITOR
                    Debug.LogFormat("mouseStartPosition {0} Input.mousePosition {1}", mouseStartPosition, Input.GetTouch(0).position);
#else
                    Debug.LogFormat("mouseStartPosition {0} Input.mousePosition {1}", mouseStartPosition, Input.mousePosition);
#endif
                    MovePlayer(diff);
                }
            }
            else
            {
                mouseDown = false;
            }

#if UNITY_EDITOR
            if (Input.GetKeyDown("left"))
            {
                MovePlayer(new Vector2(-1f, 0f));
            }
            if (Input.GetKeyDown("right"))
            {
                MovePlayer(new Vector2(1f, 0f));
            }
            if (Input.GetKeyDown("up"))
            {
                MovePlayer(new Vector2(0f, 1f));
            }
            if (Input.GetKeyDown("down"))
            {
                MovePlayer(new Vector2(0f, -1f));
            }
#endif

        }
        else if(gameState == GameState.IN_GAME_MOVE)
        {
            if (playerX != targetX || playerY != targetY)
            {
                playerX += diffX;
                playerY += diffY;

                PlacePlayer(playerX, playerY);
            }
            else
                SetGameState(GameState.IN_GAME_LEVEL_CHECK);
        }
        else if(gameState == GameState.IN_GAME_LEVEL_COMPLETE)
        {
            levelCompleteTimer -= Time.deltaTime;
            if(levelCompleteTimer <= 0.0f)
            {
                levelComplete.SetActive(false);
                NextLevel(levelSeeds[currentLevel]);
            }
        }
    }

    void MovePlayer(Vector3 diff)
    {
        Debug.LogFormat("MovePlayer({0})", diff);

        targetX = playerX;
        targetY = playerY;

        diffX = Mathf.Abs(diff.x) >= 1f ? (int)diff.x / (int)Mathf.Abs(diff.x) : 0;
        diffY = Mathf.Abs(diff.y) >= 1f ? (int)diff.y / (int)Mathf.Abs(diff.y) : 0;

        Debug.LogFormat("1 - diffX {0} diffY {1}", diffX, diffY);

        if (diffX == 0 && diffY == 0)
            return;

        diffX = (Mathf.Abs(diff.x) > Mathf.Abs(diff.y)) ? diffX : 0;
        diffY = (Mathf.Abs(diff.y) >= Mathf.Abs(diff.x)) ? diffY : 0;

        Debug.LogFormat("2 - diffX {0} diffY {1}", diffX, diffY);

        do
        {
            targetX = targetX += diffX;
            targetY = targetY += diffY;
        }
        while (IsValidMove(targetX, targetY));
        targetX -= diffX;
        targetY -= diffY;

        Debug.LogFormat("player {0},{1} target {2},{3}", playerX, playerY, targetX, targetY);

        SetGameState(GameState.IN_GAME_MOVE);
    }
}
